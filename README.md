# DESCRIPTION

This is a php trait for Direct Injection.
Stable Release: 0.3.0

## Usage:

```
<?php

// Dependencies to be used
class ClassA {
	function echo() {
		echo "A";
	};
}
class ClassB {
	function echo() {
		echo "B";
	};
}

class YourClass
{
	use Baxtian\DependencyInjectionTrait;

	// Create class constructor with a variable that will 
	// contain -if any- the list of arguments
	public function __construct($arguments = [])
	{
		// Array of attributes linked to the class
		$classes = [
			'class_a' => ClassA::class,
			'class_b' => ClassB::class,
		];

		$this->setDependencies($arguments, $classes);
	}

	// Function that uses an injected dependency
	public function foo() {
		$this->dependency('class_a')->echo();
		$this->dependency('class_b')->echo();
	}
}
```

To pass a specific instance (as a mock for testing)

```
$foo = new YourClass([
	'class_a' => new ClassA(), 
	'class_b' => new ClassB()
]);
```

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

## 0.3.1

-	In case of using this class in multiple providers, allow Composer to set which file to use by default.

## 0.3.0

-   First stable release
