<?php

namespace Baxtian;

trait DependencyInjectionTrait
{
	protected $dependencies_def;
	protected $dependencies_ins = [];

	/**
	 * Constructor del importador
	 */
	protected function setDependencies($arguments = [], $clases = [])
	{
		$this->dependencies_def = $clases;
		foreach ($this->dependencies_def as $key => $class) {
			$this->dependencies_ins[$key] = ($arguments[$key]) ?? null;
		}
	}

	private function dependency($dependencia)
	{
		// Si ya tenemos valor, retornarlo
		if (!empty($this->dependencies_ins[$dependencia])) {
			return $this->dependencies_ins[$dependencia];
		}

		if (isset($this->dependencies_def[$dependencia])) {
			$clase                                = $this->dependencies_def[$dependencia];
			$this->dependencies_ins[$dependencia] = new $clase();

			return $this->dependencies_ins[$dependencia];
		}

		return false;
	}
}
